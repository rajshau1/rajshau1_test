#
# Cookbook:: my_super_duper_cookbook
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.
package 'net-tools'
package 'emacs'
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

